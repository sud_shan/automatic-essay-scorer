
import Orange
import enchant
import string
import MySQLdb
import nltk
import textmining
import ner
import re
data = Orange.data.Table("temp1.tab")

lin = Orange.regression.linear.LinearRegressionLearner()
model=lin(data)
fo =open("essay_file.txt","r")
essay=fo.read()
#print model


tagger = ner.SocketNER(host='localhost', port=8080)
string1=essay
x_tag=tagger.get_entities(essay)
#print x_tag
x1=int (0)
y1=int(0)
z1=int(0)
for a in x_tag:
  
  if(a=="ORGANIZATION"):
    for b in x_tag[a]:
      x1=x1+1
      string1=string1.replace(b,'@ORGANIZATION'+str(x1))
      
  if(a=="LOCATION"):
    for b in x_tag[a]:
      y1=y1+1
      string1=string1.replace(b,'@LOCATION'+str(y1))
  if(a=="PERSON"):
    for b in x_tag[a]:
      z1=z1+1
      string1=string1.replace(b,'@PERSON'+str(z1))
essay=str(string1)
out = essay.translate(string.maketrans("",""), string.punctuation)
tokens = nltk.word_tokenize(out)
words=len(tokens)
x=nltk.pos_tag(tokens)
#print x
adj=0
adv=0
verb=0
noun=0
pre=0
for a in x:
	if(a[1]=="JJ"):
		adj=adj+1
	if(a[1]=="RB"):
		adv=adv+1
	if(a[1]=="NN"):
		noun=noun+1
	if(a[1]=="VB"):
		verb=verb+1
	if(a[1]=="IN"):
		pre=pre+1
error=0
for word in tokens:
	#word1="helo"
	d = enchant.Dict("en_US")
	if(d.check(word)):
		error1=0
	else:
		error=error+1
		word1=0
db = MySQLdb.connect("localhost","root","","aes")
cursor = db.cursor()
tdm = textmining.TermDocumentMatrix()
tdm.add_doc(essay)
i=0
y=[]
for row1 in tdm.rows(cutoff=1):
	y.append([])
	for x2 in row1:
		y[i].append(x2)
	i=i+1
counter=0
word1=0
#print y[0]
for word in y[0]:
	#print word
	sql="select * from corpus_matrix where words='%s'" %(word)
	cursor.execute(sql)
	row_count=cursor.rowcount
	if(cursor.rowcount!=0):
		res = cursor.fetchone()
		if(res[2]):
			calc=y[1][counter]/res[2]
			if(calc>0.8):
				word1=word1+1
	else:
		word1=word1+1
	counter=counter+1
# print words
# print word1
# print adj
# print adv

# print verb
# print noun
# print pre
# print error

sql="select * from temp_data1 data where bog between '%s' and '%s' and words between '%s' and '%s' and spelling_error between '%s' and '%s' and adjective between '%s' and '%s' and adverb between '%s' and '%s'  " %(word1-5,word1+5,words-20,words+20,error-10,error+10,adj-10,adj+10,adv-10,adv+10)
cursor.execute(sql)
rowc=cursor.rowcount
#print "Number of records similar to the given essay"
print rowc
sql="select * from temp_data1" 
cursor.execute(sql)
rowc1=cursor.rowcount
ins=[word1,words,error,adj,adv,noun,verb,pre,0]
# #for ins in data[:5]:
#print ins
score_pre=model(ins);
print " %3.2f " % (score_pre )
if(rowc>=25):
	fobj = open("temp1.tab",'a')
	fobj.write("\n"+str(word1)+"\t"+str(words)+"\t"+str(error)+"\t"+str(adj)+"\t"+str(adv)+"\t"+str(noun)+"\t"+str(verb)+"\t"+str(pre)+"\t"+str(score_pre))
	sql="insert into  temp_data1(essay_id,set1,bog,words,spelling_error,adjective,adverb,noun,verb,preposition,rating) values('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s') " %(rowc1,1,word1,words,error,adj,adv,noun,verb,pre,score_pre)
	cursor.execute(sql)
	db.commit()
# print "pred obs"
# for d in data[:3]:
#     print "%.1f %.1f" % (model(d), d.get_class())
# lin.name = "lin"
# rf = Orange.ensemble.forest.RandomForestLearner()
# rf.name = "rf"
# tree = Orange.regression.tree.TreeLearner(m_pruning = 2)
# tree.name = "tree"

learners = [lin]

res = Orange.evaluation.testing.cross_validation(learners, data, folds=5)
rmse = Orange.evaluation.scoring.RMSE(res)

#print "Learner  RMSE"
# for i in range(len(learners)):
#     print "{0:8}".format(learners[i].name),
print "%.2f" % rmse[0]

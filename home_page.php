
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">

  <title>MoneyWise

  </title>

  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.css" rel="stylesheet">

  <!-- Just for debugging purposes. Don't actually copy this line! -->
  <!--[if lt IE 9]><script src="../../docs-assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
      <![endif]-->

      <!-- Custom styles for this template -->
      <link href="css/carousel.css" rel="stylesheet">
    </head>
<!-- NAVBAR
  ================================================== -->
  <body>


    <div class="navbar-wrapper">
      <div class="container">

        <div class="navbar navbar-inverse navbar-static-top" role="navigation">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="#">MoneyWise</a>
            </div>
            <div class="navbar-collapse collapse">
              <ul class="nav navbar-nav">
                <li ><a href="#">Home</a></li>
                <!-- <li><a href="#about">About</a></li>
                <li><a href="#contact">Contact</a></li>

                 --><li><a href="#signup" data-toggle="modal"  >Sign Up</a></li>
             
                <li><a href="#login" data-toggle="modal"  >Login</a></li>
                
                <li>
             <a href="http://www.money-wizards.com" style="left:600px"  >Money-Wizards</a></li>
              
              </ul>
            </div>
          </div>
        </div>
        

      </div>
    </div>

    <div class="modal fade" id="signup" style="background:rgba(102,0,204,0.1);" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">


      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header" style="background:rgba(0,51,102,1);">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" style="color:white;" id="myModalLabel">Sign Up</h4>
          </div>
          <div class="modal-body" style="background:rgba(0,51,102,.0);">
           <?php
           if(isset($_GET['message']))
           {
            ?>
            <div class="alert alert-success alert-block">
             <a class="close" data-dismiss="alert" href="#">&times;</a>
             <h4 class="alert-heading"></h4>

             <?php
             echo htmlentities($_GET['message']);
             ?>
           </div>
           <?php
           
         }

         ?>



         <form role="form" method="post" action="register_user.php" enctype="multipart/form-data">
          <div class="form-group">
           <label for="exampleInputEmail1">Full Name</label>
           <input type="text" class="form-control" id="exampleInputEmail1" name="register_name" placeholder="Enter Full Name" required>
         </div>

         <div class="form-group">
           <label for="exampleInputEmail1">Email address</label>
           <input type="email" class="form-control" id="exampleInputEmail1" name="register_email" placeholder="Enter email" required>
         </div>
         <div class="form-group">
           <label for="username">User Name</label>
           <input type="text" class="form-control" id="username" name="register_username" placeholder="Enter User name" required>
         </div>
         
         <div class="form-group">
           <label for="exampleInputPassword1">Password</label>
           <input type="password" class="form-control" id="exampleInputPassword1" name="register_password" placeholder="Password" required>
         </div>
         <div class="form-group">
           <label for="exampleInputPassword1">Re-Enter Password</label>
           <input type="password" class="form-control" id="exampleInputPassword1" name="register_re_password" placeholder="Password" required>
         </div>
         <div class="form-group">
           <label for="exampleInputFile">Upload Your Pic </label>
           <input type="file" id="exampleInputFile" name="register_photo">
           <p class="help-block">Upload 260px X 180 px pic</p>
         </div>
         <div class="checkbox">
           <label>
             <input type="radio" name ="user_type" value="s" required>
             Signup as Student
           </label>
           <br>
           <label>
             <input type="radio" name ="user_type" value="t" required>
             Signup as Teacher
           </label>

         </div>


         <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Submit</button>
        </form>
      </div>
    </div>
  </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">


  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header"  style="background:rgba(0,51,102,1);">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" style="color:white;" id="myModalLabel">Login</h4>
      </div>
      <div class="modal-body">
       <?php 
       if(isset($_GET['message']))
       {
         ?>
         <div class="alert alert-warning alert-block">
           <a class="close" data-dismiss="alert" href="#">&times;</a>
           <h4 class="alert-heading"></h4>
           <?php echo htmlentities($_GET['message']);?>

         </div>
         <?php
       }
       ?>

       <form role="form" method="post" action="login_user.php" enctype="multipart/form-data">


        <div class="form-group">
         <label for="exampleInputEmail1">Email address</label>
         <input type="email" class="form-control" id="exampleInputEmail1" name="register_email" placeholder="Enter email" required>
       </div>
       <div class="form-group">
         <label for="exampleInputPassword1">Password</label>
         <input type="password" class="form-control" id="exampleInputPassword1" name="register_password" placeholder="Password" required>
       </div>


       <div class="checkbox">
         <label>
           <input type="radio" name ="user_type" value="s" required>
           Login as Student
         </label>
         <br>
         <label>
           <input type="radio" name ="user_type" value="t" required>
           Login as Teacher
         </label>

       </div>


       <div class="modal-footer">
       <button onclick="forgot_password()" type="button" class="btn btn-primary" >Forgot Password</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

        <button type="submit" class="btn btn-primary">Submit</button>
      </form>
    </div>
  </div>
</div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal fade" id="forgot" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">


  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header"  style="background:rgba(0,51,102,1);">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" style="color:white;" id="myModalLabel">Forgot Password</h4>
      </div>
      <div class="modal-body">
        
       

       <form role="form" method="post" action="reset_password.php" enctype="multipart/form-data">


        <div class="form-group">
         <label for="exampleInputEmail1">Email address</label>
         <input type="email" class="form-control" id="exampleInputEmail1" name="register_email" placeholder="Enter email" required>
       </div>
       


       


       <div class="modal-footer">
       
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

        <button type="submit" class="btn btn-primary">Reset Password</button>
      </form>
    </div>
  </div>
</div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
    <!-- Carousel

    ================================================== -->
    <div id="carousel">
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
        <li data-target="#myCarousel" data-slide-to="3"></li>
        <li data-target="#myCarousel" data-slide-to="4"></li>
      </ol>
      <div class="carousel-inner">
        <div class="item active">
          <img src="images/1.jpg" alt="First slide">
          <div class="container">
            <div class="carousel-caption">
            </div>
          </div>
        </div>
        <div class="item">
          <img src="images/2.jpg" alt="Second slide">
          <div class="container">
            <div class="carousel-caption">

            </div>
          </div>
        </div>
        <div class="item">
          <img src="images/3.jpg" alt="Third slide">
          <div class="container">
            <div class="carousel-caption">

            </div>
          </div>
        </div>
        <div class="item">
          <img src="images/4.jpg" alt="Fourth Slide">
          <div class="container">
            <div class="carousel-caption">

            </div>
          </div>
        </div>
        <div class="item">
          <img src="images/5.jpg" alt="Fifth Slide">
          <div class="container">
            <div class="carousel-caption">

            </div>
          </div>
        </div>
      </div>
     <!-- <a class="left carousel-control" href="#myCarousel" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
      <a class="right carousel-control" href="#myCarousel" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
     -->
  </div><!-- /.carousel -->

</div>

    <!-- Marketing messaging and featurettes
    ================================================== -->
    <!-- Wrap the rest of the page in another container to center all the content. -->

    <div class="container marketing">

      <!-- Three columns of text below the carousel -->
      <div class="row">
        <div class="col-lg-4">
          <img class="img-circle"  src="images/download1.jpg" alt="Generic placeholder image">
          <h2>Warren Buffet</h2>
          <p>Risk comes from not knowing what you're doing.
          </p>
          <!--<p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>-->
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4">
          <img class="img-circle" src="images/download2.jpg" alt="Generic placeholder image">
          <h2>Peter Lynch</h2>
          <p> I think you have to learn that there's a company behind every stock, and that there's only one real reason why stocks go up. Companies go from doing poorly to doing well or small companies grow to large companies.

          </p>
          <!-- <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>-->
         </div><!-- /.col-lg-4 -->
         <div class="col-lg-4">
          <img class="img-circle" src="images/download3.jpg" alt="Generic placeholder image">
          <h2>Charlie Munger</h2>
          <p>A great business at a fair price is superior to a fair business at a great price.
          </p>
         <!-- <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>-->
        </div><!-- /.col-lg-4 -->
      </div><!-- /.row -->


      <!-- START THE FEATURETTES -->
		<!--
      <hr class="featurette-divider">

      <div class="row featurette">
        <div class="col-md-7">
          <h2 class="featurette-heading">First featurette heading. <span class="text-muted">It'll blow your mind.</span></h2>
          <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
        </div>
        <div class="col-md-5">
          <img class="featurette-image img-responsive" data-src="holder.js/500x500/auto" alt="Generic placeholder image">
        </div>
      </div>

      <hr class="featurette-divider">

      <div class="row featurette">
        <div class="col-md-5">
          <img class="featurette-image img-responsive" data-src="holder.js/500x500/auto" alt="Generic placeholder image">
        </div>
        <div class="col-md-7">
          <h2 class="featurette-heading">Oh yeah, it's that good. <span class="text-muted">See for yourself.</span></h2>
          <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
        </div>
      </div>

      <hr class="featurette-divider">

      <div class="row featurette">
        <div class="col-md-7">
          <h2 class="featurette-heading">And lastly, this one. <span class="text-muted">Checkmate.</span></h2>
          <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
        </div>
        <div class="col-md-5">
          <img class="featurette-image img-responsive" data-src="holder.js/500x500/auto" alt="Generic placeholder image">
        </div>
      </div>

      <hr class="featurette-divider">

      <!-- /END THE FEATURETTES --> 


      <!-- FOOTER -->
      <hr>
      <footer>
        <p class="pull-right"><a href="#">Back to top</a></p>
        <p>&copy; 2014 Company, Inc. &middot; <!--<a href="#">Privacy</a> &middot; <a href="#">Terms</a></p>-->
      </footer>

    </div><!-- /.container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="bootstrap/js/holder.js"></script>
    <script type='text/javascript'>
    $(document).ready(function() {
         $('#myCarousel').carousel({
             interval: 3000
         })
    });    
</script>
<script type="text/javascript">
	function forgot_password()
	{

		$('#login').modal('hide');
		$('#forgot').modal('show');


	}
</script>
    <?php
    if(isset($_GET['message']))
    {
      ?>
      <script>
        $('#login').modal('show');
      </script>
      <?php
    }
    ?>
  </body>
  </html>
